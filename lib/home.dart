import 'package:flutter/material.dart';
import 'package:parking/detail.dart';
import 'package:parking/about.dart';

class Home extends StatelessWidget{
final posts = [
    {"title": 'SUPER PARQUEOS',
    "subtitle": 'sub'},
    {"title": 'MEGA PARKING',
    "subtitle": 'sub'},
    {"title": 'LA PAZ PARKING',
    "subtitle": 'sub'},
    {"title": 'CENTRO PARKING',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'},
    {"title": 'sss',
    "subtitle": 'sub'}
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Listado de Parqueos'),
        backgroundColor: Color(0xFF595A5B),
        
      ),
      body: Container(
        color: Color(0xFF3796C6),
        child: ListView.builder(
          itemCount: posts.length,
          padding: const EdgeInsets.all(15.0),
          itemBuilder: (context, position){
            return Column(children: <Widget>[
              
              ListTile(
                title: Text('${posts[position]["title"]}',style: TextStyle(color: Colors.white, fontSize: 25.00),),
                subtitle: Text('${posts[position]["subtitle"]}',style: TextStyle(color: Colors.white, fontSize: 16.00)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=> Detail())
                  );
                },
              ),
              Divider(height: 5.0,color: Color(0xFF595A5B)),
            ]);
          },
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.info),
        backgroundColor: Color(0xFF595A5B),
        elevation: 0.0,
        onPressed: (){
          Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=> About())
                  );
        },
      ),
    );
  }
}